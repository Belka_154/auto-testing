package org.itstep.autotesting.framevork.listener;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import org.itstep.autotesting.framevork.reporter.Reporter;

public class MethodListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            Reporter.startTest(method.getTestMethod().getMethodName(), method.getTestMethod().getDescription());
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        Reporter.endTest();
    }
}
