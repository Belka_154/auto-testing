package org.itstep.autotesting.framevork.listener;

import org.apache.logging.log4j.LogManager;
import org.itstep.autotesting.framevork.common.SystemPropertiesInitializer;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {
    public void onStart(ISuite iSuite) {
        SystemPropertiesInitializer.initSystemProperties(iSuite);
        LogManager.getLogger().info("[ SUITE STARTED ] : " + iSuite.getName());
    }

    public void onFinish(ISuite suite) {
        LogManager.getLogger().info("[SUITE FINISHED] " + suite.getName());
    }

}
