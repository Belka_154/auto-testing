package org.itstep.autotesting.framevork.ui.element;

import org.itstep.autotesting.framevork.ui.browser.Browser;

public class Folder extends BaseElement {

    public Folder(LocatorType locatorType, String locator) {
        super(locatorType, locator);
    }

    public void doubleClick() {
        Browser.getBrowser().doubleClick(by);
    }

    public void moveElementToFolder() {
        Browser.getBrowser().moveElementTo(by);
    }
}
