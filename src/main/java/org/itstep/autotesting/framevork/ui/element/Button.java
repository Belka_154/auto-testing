package org.itstep.autotesting.framevork.ui.element;

import org.itstep.autotesting.framevork.ui.browser.Browser;

public class Button extends BaseElement {
    public Button(LocatorType locatorType, String locator) {
        super(locatorType, locator);
    }

    public void click() {
        Browser.getBrowser().click(by);
    }

    public void moveToElementAndClick() {
        Browser.getBrowser().moveToElementAndClick(by);
    }
}
