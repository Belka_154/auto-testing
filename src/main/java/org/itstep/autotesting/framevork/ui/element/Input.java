package org.itstep.autotesting.framevork.ui.element;

import org.itstep.autotesting.framevork.ui.browser.Browser;

public class Input extends BaseElement {

    public Input(LocatorType locatorType, String locator) {
        super(locatorType, locator);
    }

    public void type(String text) {
        Browser.getBrowser().type(by,text);
    }

    public void typeValue(String absolutePath) {
        Browser.getBrowser().typeValue(by,absolutePath);
    }

    public void deleteText() {
        Browser.getBrowser().deleteText(by);
    }
}
