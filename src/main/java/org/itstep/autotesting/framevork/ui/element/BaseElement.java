package org.itstep.autotesting.framevork.ui.element;

import org.apache.commons.lang3.NotImplementedException;
import org.itstep.autotesting.framevork.ui.browser.Browser;
import org.openqa.selenium.By;

public abstract class BaseElement {
    protected By by;

    public BaseElement(LocatorType locatorType, String locator) {
        switch (locatorType) {
            case CSS:
                this.by = By.cssSelector(locator); break;
            case XPATH:
                this.by = By.xpath(locator); break;
                default: throw new NotImplementedException("No implemented is " + locator);
        }
    }

    public String getText() {
        return Browser.getBrowser().getText(by);
    }

    public void contextClick() {
        Browser.getBrowser().contextClick(by);
    }

    public void moveToElementAndClick() {
        Browser.getBrowser().moveToElementAndClick(by);
    }
}
