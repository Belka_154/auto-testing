package org.itstep.autotesting.framevork.ui.element;

import org.itstep.autotesting.framevork.ui.browser.Browser;

public class Picture extends BaseElement {

    public Picture(LocatorType locatorType, String locator) {
        super(locatorType, locator);
    }

    public void moveElement() {
        Browser.getBrowser().keepMouse(by);
    }
}
