package org.itstep.autotesting.framevork.ui.browser;

public enum BrowserType {
    CHROME, FIREFOX
}
