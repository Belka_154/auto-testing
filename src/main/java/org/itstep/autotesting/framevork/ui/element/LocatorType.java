package org.itstep.autotesting.framevork.ui.element;

public enum LocatorType {
    XPATH, CSS
}
