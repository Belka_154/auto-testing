package org.itstep.autotesting.framevork.ui.browser;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.itstep.autotesting.framevork.common.SystemProperties;
import org.itstep.autotesting.framevork.reporter.Reporter;
import org.itstep.autotesting.framevork.ui.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Set;

public class Browser implements WrapsDriver {
    private WebDriver driver;
    private WebDriverWait webDriverWait;
    private Logger logManager;
    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private Actions actions;

    private Browser() {
        logManager = LogManager.getLogger();
        driver = WebDriverFactory.getWebDriver();
        long explicit = Long.parseLong(System.getProperty(SystemProperties.EXPLICIT_WAIT.getSystemName()));
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(explicit).getSeconds());
        actions = new Actions(driver);
        Reporter.getReporter().reportInfo("Browser is started");
    }

    @Override
    public WebDriver getWrappedDriver() {
        return driver;
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            Logger logManager = LogManager.getLogger();
            logManager.info("Starting new browser ...");
            instance.set(new Browser());
            logManager.info("New browser started");
            Reporter.getReporter().reportInfo("Browser is started");
        }
        return instance.get();
    }

    public void open(String url) {
        logManager.info("Driver navigate to: " + url);
        this.driver.get(url);
    }

    public void close() {
        if (instance.get() != null) {
            logManager.info("Driver close");
            driver.quit();
        }
        instance.set(null);
    }

    private WebElement findElement(By by) {
        WebElement element;
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
            logManager.debug("try find element to : " + by);
            element = driver.findElement(by);
            logManager.debug("found element to : " + by);
            Reporter.getReporter().reportPass("element is found " + by);
        } catch (NoSuchElementException e) {
            logManager.debug("can not found element to : " + by);
            Reporter.getReporter().reportError("Can not find element " + by);
            screenshot();
            throw e;
        }
        return element;
    }

    public void type(By by, String text) {
        try {
            findElement(by).sendKeys(text);
            logManager.debug(String.format("type text to By:  %s text : %s", by, text));
            Reporter.getReporter().reportPass(String.format("type text to By:  %s text : %s", by, text));
        }catch (Exception e) {
            logManager.debug(String.format("can not type text to By:  %s text : %s", by, text));
            Reporter.getReporter().reportError(String.format("can not type text to By:  %s text : %s", by, text));
            screenshot();
            throw new RuntimeException(e);
        }
    }

    public void click(By by) {
        WebElement element = findElement(by);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
            logManager.debug("click to element : " + by);
            Reporter.getReporter().reportPass("click to element ", by.toString());
        } catch (Exception e) {
            logManager.debug("can not click to element ", by.toString());
            Reporter.getReporter().reportError("can not click to element ", by.toString());
            screenshot();
            throw new RuntimeException(e);
        }
    }

    public String getText(By by) {
        WebElement element = findElement(by);
        String actualText;
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
            actualText = element.getText();
            webDriverWait.until(ExpectedConditions.textToBePresentInElementLocated(by, actualText));
            logManager.debug("get text to By : " + by);
            Reporter.getReporter().reportPass("get text to element " + by);
        } catch (Exception e) {
            logManager.debug("can not get text to element " + by);
            Reporter.getReporter().reportError("can not get text to element " + by);
            screenshot();
            throw new RuntimeException(e);
        }
        return actualText;
    }

    public void switchToNewTab() {
        try {
            Set<String> windowHandles = driver.getWindowHandles();
            String currentTab = driver.getWindowHandle();
            windowHandles.remove(currentTab);
            String newTab = (String) windowHandles.toArray()[0];
            driver.close();
            driver.switchTo().window(newTab);
            logManager.debug("switch to new tab");
            Reporter.getReporter().reportPass("switch to new tab");
        } catch (Exception e){
            logManager.debug("can not switch to new tab");
            Reporter.getReporter().reportError("can not switch to new tab");
            screenshot();
            throw new RuntimeException(e);
        }
    }

    public void moveToElementAndClick(By by) {
        WebElement element = findElement(by);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
            actions.moveToElement(element)
                    .click()
                    .perform();
            logManager.debug("find element for click " + by);
            Reporter.getReporter().reportPass("find element for click " + by);
        } catch (Exception e) {
            logManager.debug("can not find element for click " + by);
            Reporter.getReporter().reportError("can not find element for click " + by);
            screenshot();
            throw new RuntimeException(e);
        }
    }

    public void deleteText(By by) {
        findElement(by).clear();
    }

    public void typeValue(By by, String text) {
        WebElement element = driver.findElement(by);
        try {
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
            element.sendKeys(text);
            logManager.debug(String.format("type text to By:  %s text : %s", by, text));
            Reporter.getReporter().reportPass (String.format("type text to By:  %s text : %s", by, text));
        } catch (Exception e) {
            logManager.debug(String.format(" can not type text to By:  %s text : %s", by, text));
            Reporter.getReporter().reportError (String.format("can not type text to By:  %s text : %s", by, text));
            screenshot();
            throw new RuntimeException(e);
        }

    }

    public void contextClick(By by) {
            actions.contextClick(findElement(by))
                    .perform();
    }

    public void keepMouse(By by) {
            actions.moveToElement(findElement(by))
                    .clickAndHold();
    }

    public void moveElementTo(By by) {
        WebElement element = findElement(by);
        try {
            actions.moveToElement(element)
                    .pause(2)
                    .release(element)
                    .perform();
            logManager.debug("move element " + by);
            Reporter.getReporter().reportPass("move element " + by);
        } catch (Exception e) {
            logManager.debug("can not move element " + by);
            Reporter.getReporter().reportError("can not move element " + by);
            screenshot();
            throw new RuntimeException(e);
        }

    }

    public void doubleClick(By by) {
        WebElement element = findElement(by);
        try {
            actions.doubleClick(element)
                    .perform();
            logManager.debug("double click to element " + by);
            Reporter.getReporter().reportPass("double click to element " + by);
        } catch (Exception e) {
            logManager.debug("can not double click to element " + by);
            Reporter.getReporter().reportError("can not double click to element " + by);
            screenshot();
            throw new RuntimeException(e);
        }

    }

    public File screenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File outputFolder = new File(System.getProperty(SystemProperties.SCREENSHOT_PATH.getSystemName()));
        String fileName = outputFolder + "/" + screenshot.getName();
        try {
            FileUtils.copyFileToDirectory(screenshot, outputFolder);
        } catch (IOException e) {
            Reporter.getReporter().reportError("Browser can not save screenshot: ");
            Reporter.getReporter().reportError(e);
            throw new RuntimeException(e);
        }
        File screenshotFile = new File(fileName);
        Reporter.getReporter().reportImage(screenshotFile);
        return screenshotFile;
    }
}

