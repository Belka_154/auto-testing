package org.itstep.autotesting.framevork.ui;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.NotImplementedException;
import org.itstep.autotesting.framevork.common.SystemProperties;
import org.itstep.autotesting.framevork.listener.WebDriverListener;
import org.itstep.autotesting.framevork.ui.browser.BrowserType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

public final class WebDriverFactory {

    private WebDriverFactory() {
    }

   public static WebDriver getWebDriver() {
    System.setProperty("wdm.cachePath",
            new File (System.getProperty(SystemProperties.DRIVER_PATH.getSystemName())).getAbsolutePath());
    String browserName = System.getProperty(SystemProperties.BROWSER_NAME.getSystemName());
    BrowserType browserType;
    try {
        browserType= BrowserType.valueOf(browserName.toUpperCase());
    }catch (Exception ex){
        throw new NotImplementedException(String.format("Browser name '%s' is incorrect implemented" + browserName));
    }

    WebDriver driver;
    switch (browserType) {
        case CHROME:
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            break;
        case FIREFOX:
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            break;
            default:
                throw new NotImplementedException("Not implemented for browser: " + browserType);
      }

    long implicitWait = Long.parseLong(System.getProperty(SystemProperties.IMPLICIT_WAIT.getSystemName()));
    WebDriver.Timeouts timeouts = driver.manage().timeouts();
    timeouts.implicitlyWait(implicitWait, TimeUnit.SECONDS);

    driver.manage().window().maximize();
       EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
       eventFiringWebDriver.register(new WebDriverListener());
       driver = eventFiringWebDriver;
    return driver;
     }
  }
