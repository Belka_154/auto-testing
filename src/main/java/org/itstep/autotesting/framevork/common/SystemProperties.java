package org.itstep.autotesting.framevork.common;

import lombok.Getter;

public enum SystemProperties {
    PRODUCT_URL ("test.selenium.baseUrl"),
    DRIVER_PATH ("test.selenium.driversPath"),
    BROWSER_NAME ("test.selenium.browserName"),
    IMPLICIT_WAIT("test.selenium.implicitWait"),
    EXPLICIT_WAIT("test.selenium.explicitWait"),
    REPORT_PATH("test.selenium.reportPath"),
    SCREENSHOT_PATH("test.selenium.screenshotePath"),
    PICTURE("test.selenium.picture");

    @Getter
    private String systemName;

    SystemProperties(String systemName) {
        this.systemName = systemName;
    }
}
