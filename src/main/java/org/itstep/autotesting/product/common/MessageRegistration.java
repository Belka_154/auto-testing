package org.itstep.autotesting.product.common;

import lombok.Getter;

public enum MessageRegistration {
    INCORRECT_PASSWORD_OR_LOGIN("Неверное имя или пароль"),
    INCORRECT_LOGIN("Неверное имя ящика"),
    INPUT_LOGIN("Введите имя ящика"),
    INPUT_PASSWORD("Введите пароль"),
    USER_EMAIL("alesia154@list.ru");

    @Getter
    private String message;

    MessageRegistration(String message) {
        this.message = message;
    }
}
