package org.itstep.autotesting.product.test;

import org.itstep.autotesting.product.page.LoginPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.itstep.autotesting.product.common.MessageRegistration.INCORRECT_LOGIN;
import static org.itstep.autotesting.product.common.MessageRegistration.INCORRECT_PASSWORD_OR_LOGIN;
import static org.itstep.autotesting.product.common.MessageRegistration.INPUT_LOGIN;
import static org.itstep.autotesting.product.common.MessageRegistration.INPUT_PASSWORD;
import static org.itstep.autotesting.product.common.MessageRegistration.USER_EMAIL;

public class MailTest extends BaseTest {

    @DataProvider(name = "data to verify correct credentials")
    public Object[][] credentialsCorrect() {
        return new Object[][] {
                {"Alesia154", "bagira111", USER_EMAIL.getMessage()},
                {"alesia154", "bagira111", USER_EMAIL.getMessage()},
                {"aLeSiA154", "bagira111", USER_EMAIL.getMessage()},
                {"    alesia154    ", "bagira111", USER_EMAIL.getMessage()}
        };
    }

    @DataProvider(name = "data to verify with invalid password")
    public Object[][] credentialsInvalidPassword() {
        return new Object[][] {
                {"alesia154", "Bagira111",INCORRECT_PASSWORD_OR_LOGIN.getMessage() },
                {"alesia154", "багира111", INCORRECT_PASSWORD_OR_LOGIN.getMessage()},
                {"alesia154", "ифпшкф111", INCORRECT_PASSWORD_OR_LOGIN.getMessage()},
                {"alesia154 ", " ", INCORRECT_PASSWORD_OR_LOGIN.getMessage()},
                {"alesia154", "", INPUT_PASSWORD.getMessage()},
                {"alesia154", " bagira111 ",INCORRECT_PASSWORD_OR_LOGIN.getMessage() },
                {"belka333", "bagira111", INCORRECT_PASSWORD_OR_LOGIN.getMessage()}
        };
    }

    @DataProvider(name = "Login data with incorrect username")
    public Object[][] credentialsInvalidLogin() {
        return new Object[][] {
                {"alesia1544", INCORRECT_LOGIN.getMessage()},
                {"alesia-154        ", INCORRECT_LOGIN.getMessage()},
                {"фдуышф154", INCORRECT_LOGIN.getMessage()},
                {"", INPUT_LOGIN.getMessage()},
                {" ", INPUT_LOGIN.getMessage()},
                {"alesia115544", INCORRECT_LOGIN.getMessage()}
        };
    }

    @Test(testName = "testMailLoginPositive",
            groups = "Login",
            description = "data verification for user logging",
            dataProvider = "data to verify correct credentials")
    public void testMailPositiveLoginUser(String login, String password, String expectedText) {
        String actualConfirmingLoginText = LoginPage.open()
                .typeLogin(login)
                .clickButtonSelection()
                .clickButtonInputPassword()
                .typePassword(password)
                .clickButtonInput()
                .getLoginConfirmingLoginText();
        Assert.assertEquals(actualConfirmingLoginText, expectedText, "verification of successful login");
    }

    @Test(testName = "testMailLoginWithPasswordNegative",
            groups = "Login",
            description = "data verification for user logging",
            dataProvider = "data to verify with invalid password")
    public void testMailNegativePassword(String login, String password, String expectedText) {
        String actualErrorMessageText = LoginPage.open()
                .typeLogin(login)
                .clickButtonSelection()
                .clickButtonInputPassword()
                .typePassword(password)
                .clickButtonInput()
                .getErrorMessageText();
           Assert.assertEquals(actualErrorMessageText, expectedText, "Verify Error message text");
    }

    @Test(testName = "testMailLoginNegative",
            groups = "Login",
            description = "data verification for user logging",
            dataProvider = "Login data with incorrect username")
    public void testMailLoginNegative(String login, String expectedText) {
       String actualErrorMessageText = LoginPage.open()
                .typeLogin(login)
                .clickButtonSelection()
                .clickButtonInputPassword()
                .getErrorMessageText();
            Assert.assertEquals(actualErrorMessageText, expectedText, "Verify Error message text");
    }
}
