package org.itstep.autotesting.product.test;

import org.itstep.autotesting.framevork.common.SystemProperties;
import org.itstep.autotesting.product.page.CloudPage;
import org.itstep.autotesting.product.page.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

public class MailCloudTest extends BaseTest {

    @DataProvider(name = "data for test mail cloud")
    public Object[][] credentialsInvalidPassword() {
        return new Object[][]{
                {"alesia154", "bagira111", "eeeeee", "ttt", "bee-on-daisy"},
        };
    }

    @Test(testName = "testMailCloud",
            groups = "cloud",
            description = "data verification for file in cloud",
    dataProvider = "data for test mail cloud")
    public void testMailCloud(String login, String password, String nameFirstFolder, String nameSecondFolder, String namePicture) {
        String absolutePath = new File(System.getProperty(SystemProperties.PICTURE.getSystemName())).getAbsolutePath();

        CloudPage cloudPage = LoginPage.open()
                .typeLogin(login)
                .clickButtonSelection()
                .clickButtonInputPassword()
                .typePassword(password)
                .clickButtonInput()
                .clickButtonCloud()
                .clickCloseAdvertising()
                .clickCreateNewFolder()
                .selectionCreateFolder()
                .deleteTextWithFolder()
                .typeNameFolder(nameFirstFolder)
                .clickButtonCreateFolder();
        String actualNameFolder = cloudPage.getNameFolder();

        softAssert.assertEquals(actualNameFolder, nameFirstFolder, "Verify name folder");
        CloudPage cloudPage1 = cloudPage.clickButtonLoading()
                .typeAbsolutePathForLoadPicture(absolutePath);
        String actualNamePicture = cloudPage1.getNamePicture();

        softAssert.assertEquals(actualNamePicture, namePicture, "Verify name picture");
        CloudPage cloudPage2 = cloudPage1.clickButtonCreateSecondFolder()
                .selectionCreateFolder()
                .deleteTextWithFolder()
                .typeNameFolder(nameSecondFolder)
                .clickButtonCreateFolder()
                .clickONFirstFolder()
                .dragPicture()
                .doubleClickOnFolder();
        String actualNamePictureInNewFolder = cloudPage2.getNamePicture();

        softAssert.assertEquals(actualNamePictureInNewFolder, namePicture, "Verify name picture");
        cloudPage2.clickRightButtonMouseOnPicture()
                .clickButtonMoveToNewFolder()
                .clickButtonChoiceFolder()
                .clickButtonChoice()
                .clickButtonRemove()
                .getLink();
        softAssert.assertAll();
    }
}
