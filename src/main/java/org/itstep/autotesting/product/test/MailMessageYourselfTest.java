package org.itstep.autotesting.product.test;

import org.itstep.autotesting.product.page.LoginPage;
import org.itstep.autotesting.product.page.MessageYourselfPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MailMessageYourselfTest extends BaseTest {

    @DataProvider(name = "data with test message yourself")
    public Object[][] credentialsInvalidLogin() {
        return new Object[][] {
                {"alesia154", "bagira111", "alesia154@list.ru", "for me", "hi Alesia", "hi Alesia -- Alesia Yeustsihneeva"}
        };
    }

    @Test(testName = "messageYourselfTest",
    groups = "message",
    description = "checking for the presence and absence of messages",
    dataProvider = "data with test message yourself")
    public void testInputMessage(String login, String password, String loginUser, String summary, String text, String expectedText) {
        MessageYourselfPage messageYourselfPage = LoginPage.open()
                .typeLogin(login)
                .clickButtonSelection()
                .clickButtonInputPassword()
                .typePassword(password)
                .clickButtonEnterAndSwithToNewTab()
                .clickButtonWriteMessage()
                .typeWhoToSend(loginUser)
                .typeSubjectMessage(summary)
                .typeTextMessage(text)
                .clickButtonSent()
                .clickButtonClose()
                .clickButtonSented();
        String actualTextSent = messageYourselfPage.getLoginUser();
        softAssert.assertEquals(actualTextSent, expectedText, "Verify that the message is in sent");

        MessageYourselfPage message = messageYourselfPage.clickButtonIncoming()
                .clickButtonLettersToMyself();
        String actualTextReceived = message.getLoginUser();
        softAssert.assertEquals(actualTextReceived, expectedText, "Verify that the message is in received");

        MessageYourselfPage mailFolder = message.clickButtonGoToYourMailFolder()
                .clickCheckbox()
                .clickButtonDelete()
                .clickButtonBasket();
        String actualTextBasket = mailFolder.getLoginUser();
        softAssert.assertEquals(actualTextBasket, expectedText, "Verify that the message is in basket");

        String actualNotTextBasket = mailFolder.clickCheckbox()
                .clickButtonDelete()
                .getLoginUser();
        softAssert.assertNotEquals(actualNotTextBasket, expectedText, "Verify that the message is't in basket");
        softAssert.assertAll();
    }
}
