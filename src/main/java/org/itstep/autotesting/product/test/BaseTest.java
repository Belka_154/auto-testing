package org.itstep.autotesting.product.test;

import org.itstep.autotesting.framevork.ui.browser.Browser;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public abstract class BaseTest {

    protected SoftAssert softAssert;

    @BeforeMethod(alwaysRun = true)
    public void initMethod() {
        softAssert = new SoftAssert();
    }

    @AfterMethod(alwaysRun = true)
    public void teardown() {
        Browser.getBrowser().close();
    }
}
