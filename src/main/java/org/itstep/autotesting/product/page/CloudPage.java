package org.itstep.autotesting.product.page;

import org.itstep.autotesting.framevork.ui.element.Button;
import org.itstep.autotesting.framevork.ui.element.Folder;
import org.itstep.autotesting.framevork.ui.element.Input;
import org.itstep.autotesting.framevork.ui.element.Message;
import org.itstep.autotesting.framevork.ui.element.Picture;

import static org.itstep.autotesting.framevork.ui.element.LocatorType.XPATH;

public class CloudPage {

    // locators
    private static final String CLOSE_ADVERTISING_XPATH_LOCATOR = "//*[@class='Dialog__close--1rKyk']";
    private static final String BUTTON_CREATE_NEW_DOCUMENT_OR_FOLDER_XPATH_LOCATOR = "//*[@class='DataListItemCreateNew__root--3OmvE DataListItemCreateNew__rootThumb--2zkxa']";
    private static final String SELECTION_CREATE_FOLDER_XPATH_LOCATOR ="//*[@data-name='createFolder']";
    private static final String FOLDER_TEXT_NEW_FOLDER_XPATH_LOCATOR = "//*[@class='ui fluid focus input']/input";
    private static final String BUTTON_CREATE_FOLDER_XPATH_LOCATOR ="//*[@class='ui fluid primary button']";
    private static final String NAME_FOLDER_XPATH_LOCATOR = "//*[@data-id='/eeeeee']";
    private static final String BUTTON_LOADING_XPATH_LOCATOR = "//*[@id='cloud_toolbars']//*[@data-name ='upload']";
    private static final String INPUT_FOR_ABSOLUTE_PATH_PICTURE_XPATH_LOCATOR = "//*[@class='layer_upload__controls__input']";
    private static final String TEXT_PICTURE_XPATH_LOCATOR = "//*[@class='Name__name--13u3t']";
    private static final String BUTTON_CREATE_SECOND_FOLDER = "//*[@data-name='create']";
    private static final String LOCATION_PICTURES_XPATH_LOCATOR = "//*[@class='DataListItemThumb__thumb--20KgA']";
    private static final String FIRST_FOLDER_TO_DRAG_THE_PICTURE_XPATH_LOCATOR = "//*[@class='DataListItemThumb__content--2Puyh']";
    private static final String MOVE_IN_NEW_FOLDER_XPATH_LOCATOR = "//*[@data-name='move']";
    private static final String FIRST_FOLDER_XPATH_LOCATOR ="//*[@class='b-nav__item js-href']";
    private static final String BUTTON_MOVE_TO_NEW_FOLDER_XPATH_LOCATOR = "//*[@data-name='move']/div";
    private static final String BUTTON_CHOICE_FOLDER_XPATH_LOCATOR = "//*[@data-name='/eeeeee']";
    private static final String BUTTON_CHOICE_XPATH_LOCATOR = "//*[@data-name='action']";
    private static final String BUTTON_REMOVE_XPATH_LOCATOR = "//*[@data-name='remove']/div[1]";
    private static final String BUTTON_REMOVE_CONFIRMATION_XPATH_LOCATOR = "//*[@class='btn btn_main btn_neighboring ']";
    private static final String BUTTON_LINC_ACCES_XPATH_LOCATOR = "//*[@data-name='publish']/div[@class='DropdownItemAction__root--2vZQy']";
    private static final String ELEMENT_COPY_LINC_XPATH_LOCATOR = "//*[@class='ShadeRight__root--DXSrE']";

    //elements
    private static Button buttonCloseAdvertising = new Button(XPATH, CLOSE_ADVERTISING_XPATH_LOCATOR);
    private static Button hoverAndClickOnElementCreateNewFolderOrFile = new Button(XPATH, BUTTON_CREATE_NEW_DOCUMENT_OR_FOLDER_XPATH_LOCATOR);
    private static Button buttonSelectionCreateFolder = new Button(XPATH, SELECTION_CREATE_FOLDER_XPATH_LOCATOR);
    private static Input deleteTextWithFolder = new Input (XPATH, FOLDER_TEXT_NEW_FOLDER_XPATH_LOCATOR);
    private static Input typeNewNameFolder = new Input (XPATH, FOLDER_TEXT_NEW_FOLDER_XPATH_LOCATOR);
    private static Button buttonCreateFolder = new Button(XPATH, BUTTON_CREATE_FOLDER_XPATH_LOCATOR);
    private static Message getNameFolder = new Message(XPATH, NAME_FOLDER_XPATH_LOCATOR);
    private static Button buttonLoading = new Button(XPATH, BUTTON_LOADING_XPATH_LOCATOR);
    private static Input inputAbsolutePathForLoadPicture = new Input(XPATH, INPUT_FOR_ABSOLUTE_PATH_PICTURE_XPATH_LOCATOR);
    private static Message getNameImage = new Message(XPATH, TEXT_PICTURE_XPATH_LOCATOR);
    private static Button buttonCreateSecondFolder = new Button(XPATH, BUTTON_CREATE_SECOND_FOLDER);
    private static Picture clickOnPicture = new Picture(XPATH, LOCATION_PICTURES_XPATH_LOCATOR);
    private static Folder folderForDragPicture = new Folder(XPATH, FIRST_FOLDER_TO_DRAG_THE_PICTURE_XPATH_LOCATOR);
    private static Button buttonDragInNewFolder = new Button(XPATH, MOVE_IN_NEW_FOLDER_XPATH_LOCATOR);
    private static Button buttonFirstFolder = new Button(XPATH, FIRST_FOLDER_XPATH_LOCATOR);
    private static Button buttonMoveToNewFolder = new Button(XPATH, BUTTON_MOVE_TO_NEW_FOLDER_XPATH_LOCATOR);
    private static Button buttonSecondFolder = new Button(XPATH, BUTTON_CHOICE_FOLDER_XPATH_LOCATOR);
    private static Button buttonMove = new Button(XPATH, BUTTON_CHOICE_XPATH_LOCATOR);
    private static Button buttonRemove = new Button(XPATH, BUTTON_REMOVE_XPATH_LOCATOR);
    private static Button buttonRemoveConfirmation = new Button(XPATH, BUTTON_REMOVE_CONFIRMATION_XPATH_LOCATOR);
    private static Folder choiceFolderForGetLink = new Folder(XPATH, NAME_FOLDER_XPATH_LOCATOR);
    private static Button buttonLinkAcces = new Button(XPATH, BUTTON_LINC_ACCES_XPATH_LOCATOR);
    private static Input copyLinkWithInputField = new Input(XPATH, ELEMENT_COPY_LINC_XPATH_LOCATOR);

    public CloudPage clickCloseAdvertising() {
        buttonCloseAdvertising.click();
        return this;
    }

    public CloudPage clickCreateNewFolder() {
        hoverAndClickOnElementCreateNewFolderOrFile.moveToElementAndClick();
        return this;
    }

    public CloudPage selectionCreateFolder() {
        buttonSelectionCreateFolder.moveToElementAndClick();
        return this;
    }

    public CloudPage deleteTextWithFolder() {
        deleteTextWithFolder.deleteText();
        return this;
    }

    public CloudPage typeNameFolder(String nameFolder) {
        typeNewNameFolder.type(nameFolder);
        return this;
    }

    public CloudPage clickButtonCreateFolder() {
        buttonCreateFolder.click();
        return this;
    }

    public String getNameFolder() {
        return getNameFolder.getText();
    }

    public CloudPage clickButtonLoading() {
        buttonLoading.click();
        return this;
    }

    public CloudPage typeAbsolutePathForLoadPicture(String absolutePath) {
        inputAbsolutePathForLoadPicture.typeValue(absolutePath);
        return this;
    }

    public String getNamePicture() {
        return getNameImage.getText();
    }

    public CloudPage clickButtonCreateSecondFolder() {
        buttonCreateSecondFolder.click();
        return this;
    }


    public CloudPage dragPicture() {
        clickOnPicture.moveElement();
        folderForDragPicture.moveElementToFolder();
        buttonDragInNewFolder.click();
        return this;
    }

    public CloudPage doubleClickOnFolder() {
        folderForDragPicture.doubleClick();
        return this;
    }

    public CloudPage clickRightButtonMouseOnPicture() {
        clickOnPicture.contextClick();
        return this;
    }

    public CloudPage clickONFirstFolder() {
        buttonFirstFolder.moveToElementAndClick();
        return this;
    }

    public CloudPage clickButtonMoveToNewFolder() {
        buttonMoveToNewFolder.click();
        return this;
    }

    public CloudPage clickButtonChoiceFolder() {
        buttonSecondFolder.moveToElementAndClick();
        return this;
    }

    public CloudPage clickButtonChoice() {
        buttonMove.click();
        return this;
    }

    public CloudPage clickButtonRemove() {
        buttonRemove.click();
        buttonRemoveConfirmation.moveToElementAndClick();
        return this;
    }

    public CloudPage getLink() {
        choiceFolderForGetLink.contextClick();
        buttonLinkAcces.click();
        copyLinkWithInputField.moveToElementAndClick();
        return this;
    }
}
