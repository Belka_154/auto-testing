package org.itstep.autotesting.product.page;

import org.itstep.autotesting.framevork.common.SystemProperties;
import org.itstep.autotesting.framevork.ui.browser.Browser;
import org.itstep.autotesting.framevork.ui.element.Button;
import org.itstep.autotesting.framevork.ui.element.Input;
import org.itstep.autotesting.framevork.ui.element.Message;

import static org.itstep.autotesting.framevork.ui.element.LocatorType.XPATH;

public class LoginPage {

    // locators
    private static final String ERROR_MESSAGE_XPATH_LOCATOR = "//*[@id='mailbox:error']";
    private static final String LOGIN_CONFIRMING_LOGIN_XPATH_LOCATOR = "//*[@id='PH_user-email']";
    private static final String LOGIN_INPUT_XPATH_LOCATOR = "//*[@id='mailbox:login']";
    private static final String SELECTION_BUTTON_XPATH_LOCATOR = "//*[@id='mailbox:domain']//option[3]";
    private static final String BUTTON_INPUT_PASSWORD_XPATH_LOCATOR = "//*[@id='mailbox:submit']";
    private static final String PASSWORD_INPUT_XPATH_LOCATOR = "//*[@id='mailbox:password']";
    private static final String BUTTON_INPUT_XPATH_LOCATOR = "//*[@id='mailbox:submit']";
    private static final String BUTTON_CLOUD_CSS_SELECTOR = "//*[@class='sidebar__footer js-tooltip-direction_right sidebar__footer_menu-list sidebar__footer_loaded']/div[1]";

    //elements
    private static Message messageFailed = new Message(XPATH,ERROR_MESSAGE_XPATH_LOCATOR);
    private static Message messagePositive = new Message(XPATH, LOGIN_CONFIRMING_LOGIN_XPATH_LOCATOR);
    private static Input loginInput = new Input(XPATH, LOGIN_INPUT_XPATH_LOCATOR);
    private static Button selectionButton = new Button(XPATH, SELECTION_BUTTON_XPATH_LOCATOR);
    private static Button inputPasswordButton = new Button(XPATH,BUTTON_INPUT_PASSWORD_XPATH_LOCATOR);
    private static Input passwordInput = new Input(XPATH, PASSWORD_INPUT_XPATH_LOCATOR);
    private static Button inputButton = new Button(XPATH, BUTTON_INPUT_XPATH_LOCATOR);
    private static Button cloudButton = new Button(XPATH, BUTTON_CLOUD_CSS_SELECTOR);

    public static LoginPage open() {
        Browser.getBrowser().open(System.getProperty(SystemProperties.PRODUCT_URL.getSystemName()));
        return new LoginPage();
    }

    public LoginPage typeLogin(String login) {
        loginInput.type(login);
        return this;
    }

    public LoginPage clickButtonSelection() {
        selectionButton.click();
        return this;
    }

    public LoginPage clickButtonInputPassword() {
        inputPasswordButton.click();
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordInput.type(password);
        return this;
    }

    public LoginPage clickButtonInput() {
        inputButton.click();
        return this;
    }

    public MessageYourselfPage clickButtonEnterAndSwithToNewTab() {
        inputButton.click();
        return new MessageYourselfPage();
    }

    public CloudPage clickButtonCloud() {
        cloudButton.click();
        Browser.getBrowser().switchToNewTab();
        return new CloudPage();
    }

    public String getErrorMessageText() {
        return messageFailed.getText();
    }

    public String getLoginConfirmingLoginText() {
        return messagePositive.getText();
    }
}
