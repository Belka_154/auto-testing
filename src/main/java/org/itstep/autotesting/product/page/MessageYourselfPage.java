package org.itstep.autotesting.product.page;

import org.itstep.autotesting.framevork.ui.element.Button;
import org.itstep.autotesting.framevork.ui.element.Input;
import org.itstep.autotesting.framevork.ui.element.Message;

import static org.itstep.autotesting.framevork.ui.element.LocatorType.XPATH;

public class MessageYourselfPage {
    //locators
    private static final String BUTTON_WRITE_MESSAGE_XPATH_LOCATOR = "//*[@class='sidebar__compose-btn-box sidebar__compose-btn-box_with-dropdown']/a";
    private static final String INPUT_WHO_TO_SEND_XPATH_LOCATOR_XPATH_LOCATOR = "//*[@class='input--3slxg']//input[@class='container--H9L5q size_s--3_M-_']";
    private static final String INPUT_LETTER_SUBJECT_XPATH_LOCATOR = "//*[@class='container--3QXHv']//input[@class='container--H9L5q size_s--3_M-_']";
    private static final String INPUT_TEXT_MESSAGE_XPATH_LOCATOR = "//*[@role='textbox']/div";
    private static final String BUTTON_SEND_XPATH_LOCATOR = "//*[@class='button2 button2_base button2_primary button2_hover-support js-shortcut']";
    private static final String BUTTON_CLOSE_XPATH_LOCATOR = "//*[@class='ico ico_16-close ico_size_s']";
    private static final String BUTTON_SENT_XPATH_LOCATOR = "//*[@class='nav nav_expanded nav_hover-support']/a[5]";
    private static final String LOGIN_USER_XPATH_LOCATOR ="//*[@class='ll-sp__normal']";
    private static final String BUTTON_INCOMING_XPATH_LOCATOR = "//*[@class='nav__folder nav__folder_parent']/div[@class='nav__folder-name']";
    private static final String BUTTON_LETTERS_TO_MYSELF_XPATH_LOCATOR = "//*[@class='mt-t mt-t_tomyself mt-t_unread']";
    private static final String BUTTON_GO_TO_YOUR_MAIL_FOLDER_XPATH_LOCATOR = "//*[@class='metathread-footer']//a";
    private static final String CHECKBOX_XPATH_LOCATOR = "//*[@class='ll-av__img']";
    private static final String BUTTON_DELETE_XPATH_LOCATOR ="//*[@data-title-shortcut='Del']";
    private static final String BUTTON_BASKET_XPATH_LOCATOR ="//div[@class='nav__folder-clear']/preceding-sibling::div[1]";
    //elements
    private static Button buttonWriteMessage = new Button(XPATH, BUTTON_WRITE_MESSAGE_XPATH_LOCATOR);
    private static Input inputWhoToSend = new Input(XPATH, INPUT_WHO_TO_SEND_XPATH_LOCATOR_XPATH_LOCATOR);
    private static Input inputLetterSubject = new Input(XPATH, INPUT_LETTER_SUBJECT_XPATH_LOCATOR);
    private static Input inputTextMessage = new Input(XPATH, INPUT_TEXT_MESSAGE_XPATH_LOCATOR);
    private static Button buttonSend = new Button(XPATH, BUTTON_SEND_XPATH_LOCATOR);
    private static Button buttonClose = new Button(XPATH, BUTTON_CLOSE_XPATH_LOCATOR);
    private static Button buttonSent = new Button(XPATH, BUTTON_SENT_XPATH_LOCATOR);
    private static Message hasUserLogin = new Message(XPATH, LOGIN_USER_XPATH_LOCATOR);
    private static Button buttonIncoming = new Button(XPATH, BUTTON_INCOMING_XPATH_LOCATOR);
    private static Button buttonLettersToMyself = new Button(XPATH, BUTTON_LETTERS_TO_MYSELF_XPATH_LOCATOR);
    private static Button buttonGoToYourMailFolder = new Button(XPATH, BUTTON_GO_TO_YOUR_MAIL_FOLDER_XPATH_LOCATOR);
    private static Button checkbox = new Button(XPATH, CHECKBOX_XPATH_LOCATOR);
    private static Button buttonDelete = new Button(XPATH, BUTTON_DELETE_XPATH_LOCATOR);
    private static Button buttonBasket = new Button(XPATH, BUTTON_BASKET_XPATH_LOCATOR);

    public MessageYourselfPage clickButtonWriteMessage() {
        buttonWriteMessage.click();
        return this;
    }

    public MessageYourselfPage typeWhoToSend(String when) {
        inputWhoToSend.type(when);
        return this;
    }

    public MessageYourselfPage typeSubjectMessage(String tema) {
        inputLetterSubject.type(tema);
        return this;
    }

    public MessageYourselfPage typeTextMessage(String text) {
        inputTextMessage.type(text);
        return this;
    }

    public MessageYourselfPage clickButtonSent(){
        buttonSend.click();
        return this;
    }

    public MessageYourselfPage clickButtonClose() {
        buttonClose.click();
        return this;
    }

    public MessageYourselfPage clickButtonSented(){
        buttonSent.click();
        return this;
    }

    public String getLoginUser() {
        return hasUserLogin.getText();
    }

    public MessageYourselfPage clickButtonIncoming() {
        buttonIncoming.click();
        return this;
    }

    public MessageYourselfPage clickButtonLettersToMyself(){
        buttonLettersToMyself.click();
        return this;
    }

    public MessageYourselfPage clickButtonGoToYourMailFolder() {
        buttonGoToYourMailFolder.click();
        return this;
    }

    public MessageYourselfPage clickCheckbox() {
        checkbox.click();
        return this;
    }

    public MessageYourselfPage clickButtonDelete() {
        buttonDelete.click();
        return this;
    }

    public MessageYourselfPage clickButtonBasket() {
        buttonBasket.click();
        return this;
    }
}
